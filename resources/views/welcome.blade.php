<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>CRUD-LARAVEL-VUEJS</title>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <script src="https://kit.fontawesome.com/6bc659da2c.js" crossorigin="anonymous"></script>
        
    </head>
    <body class="antialiased">
        <div id="app">
        </div> 
    </body>
    <script  src="{{ asset('js/app.js') }}"></script>
</html>
